import sqlite3

from db.clients import BaseDatabaseClient


class SqliteClient(BaseDatabaseClient):

    def __init__(self, database_info):
        self.db_name = database_info['name']

    def connect(self):
        return sqlite3.connect(self.db_name)
