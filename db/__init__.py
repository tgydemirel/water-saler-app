from db.utils import import_string


# todo add settings to project and move these blocks to there
DATABASE = {
    "client": "sqlite",
    "name": "water_sales",
    "user": "saler",
}

CLIENT_MAPPING = {
    "sqlite": "db.clients.sqlite.SqliteClient"
}


def get_db_client_class():
    client = DATABASE.get("client", "sqlite")
    return import_string(CLIENT_MAPPING[client])


def get_db_client():
    klass = import_string(get_db_client_class())
    return klass(DATABASE)
