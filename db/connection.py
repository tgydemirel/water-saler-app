from db import DATABASE, get_db_client_class


def connect_db():
    db_conn = DBConnection()
    connection1 = db_conn.get_connection()
    connection2 = db_conn.get_connection()
    if connection1 == connection2:
        print('successfully connected.')


class DBConnection:
    connection = None

    def get_connection(self, new=False):
        """Creates return new Singleton database connection"""
        if new or not self.connection:
            print('in creation method')
            client = get_db_client_class()(DATABASE)
            self.connection = client.connect()
        return self.connection

    @classmethod
    def execute_query(cls, query):
        # todo refactor this method
        """execute query on singleton db connection"""
        connection = cls.get_connection()
        try:
            cursor = connection.cursor()
        except pyodbc.ProgrammingError:
            connection = cls.get_connection(new=True)  # Create new connection
            cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        cursor.close()
        return result
