from decimal import Decimal
from typing import List

from db.manager import DatabaseManager


class Model:
    objects = DatabaseManager

    def save(self):
        pass


class Consumer(Model):
    id: str = None
    first_name: str = None
    last_name: str = None
    phone_number: str = None
    address: str = None
    order_count: int = None


class Product:
    id: str = None
    name: str = None
    price: Decimal = None


class Order:
    id: str = None
    consumer: Consumer = None
    products: List[Product] = None
    status: str = None

    @property
    def total_amount(self):
        total_amount = Decimal(0)
        for product in self.products:
            total_amount += product.price
        return total_amount
